
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../controllers/favorite_food_controller.dart';
import '../../models/food.dart';

class FavoriteWidget extends StatefulWidget {
  final AbstractFood _food;
  const FavoriteWidget(this._food, {Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _FavoriteState();

}

class _FavoriteState extends State<FavoriteWidget> {


  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: widget._food,
      builder: (context, isFavorite) {
        return InkWell(
            child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: widget._food.isFavorite
                    ? const Icon(
                  Icons.favorite,
                  color: Colors.red,
                )
                    : const Icon(Icons.favorite_border_outlined)),
            borderRadius: const BorderRadius.all(Radius.circular(20.0)),
            onTap: () {
              FavoriteFoodController().reverseFavorite(widget._food);
            }
        );
      },
    );
  }

}