import 'package:cached_network_image/cached_network_image.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:project_test/models/food.dart';

import '../../controllers/favorite_food_controller.dart';
import 'food_widget.dart';

class FoodPage extends StatelessWidget {
  final AbstractFood food;

  FoodPage({required this.food});

  @override
  Widget build(BuildContext context) {
    return AbstractFoodWidget(food);
  }


}
