import 'package:cached_network_image/cached_network_image.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:project_test/models/food.dart';
import 'package:project_test/responsive.dart';

import 'favorite_widget.dart';
import 'food_page.dart';

abstract class AbstractFoodWidget extends StatelessWidget {
  final AbstractFood _food;
  const AbstractFoodWidget._(this._food, {Key? key}) : super(key: key);

  factory AbstractFoodWidget(AbstractFood food) {
    if (food is Food) {
      return FoodWidget(food);
    } else if (food is FoodCompilation) {
      return FoodCompilationWidget(food);
    } else {
      throw UnimplementedError();
    }
  }
}

class FoodWidget extends AbstractFoodWidget {
  const FoodWidget(Food food, {Key? key}) : super._(food, key: key);

  @override
  Widget build(BuildContext context) {
    return _buildFoodInfo(context);
  }

  Widget _buildFoodInfo(BuildContext context) {
    return Responsive.isMobile(context)
        ? _buildMobile(context)
        : _buildDesktop(context);
  }

  Widget _buildMobile(BuildContext context) {
    return CustomScrollView(
      slivers: [
        SliverAppBar(
          expandedHeight: 400,

          flexibleSpace: FlexibleSpaceBar(
            title: Text(
              _food.title,
              textAlign: TextAlign.center,
            ),
            background: CachedNetworkImage(
              imageUrl: _food.thumbnail_url,
              fit: BoxFit.cover,
            ),
          ),
          //floating: true,
          pinned: true,
          //automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () => Navigator.pop(context),
            icon: Icon(Icons.arrow_back),
          ),
          actions: [FavoriteWidget(_food)],
        ),
        SliverToBoxAdapter(
          child: Padding(
            padding: const EdgeInsets.only(bottom: 16),
            child: InkWell(
              splashColor: Colors.transparent,
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(Radius.circular(16.0)),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.6),
                        offset: const Offset(4, 4),
                        blurRadius: 16,
                      )
                    ]),
                child: Container(
                    color: Colors.white,
                    child: Column(children: [
                      _buildExpandablePanel(
                          context,
                          'Ingredients',
                          (_food as Food)
                              .ingredientsList
                              .ingredients
                              .join("\n")),
                      const Divider(
                        thickness: 2,
                        height: 1,
                      ),
                      _buildExpandablePanel(
                          context, 'Recipe', (_food as Food).recipe.recipe)
                    ])),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildDesktop(BuildContext context) {
    return CustomScrollView(
      slivers: [
        SliverAppBar(
          flexibleSpace: FlexibleSpaceBar(
            title: Text(
              _food.title,
              textAlign: TextAlign.center,
            ),
          ),
          //floating: true,
          pinned: true,
          //automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () => Navigator.pop(context),
            icon: Icon(Icons.arrow_back),
          ),
        ),
        SliverToBoxAdapter(
          child: Padding(
            padding: const EdgeInsets.only(bottom: 16),
            child: InkWell(
              splashColor: Colors.transparent,
              child: Container(
                  decoration: BoxDecoration(
                      borderRadius:
                          const BorderRadius.all(Radius.circular(16.0)),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.white.withOpacity(0.6),
                          offset: const Offset(4, 4),
                          blurRadius: 16,
                        )
                      ]),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Text(
                                'Ingredients',
                                style: TextStyle(
                                    fontSize: Theme.of(context)
                                        .textTheme
                                        .headline6
                                        ?.fontSize),
                              ),
                              Text(
                                '⏺' +
                                    (_food as Food)
                                        .ingredientsList
                                        .ingredients
                                        .join('\n⏺'),
                                textAlign: TextAlign.left,
                              ),
                              Text(
                                'Recipe',
                                style: TextStyle(
                                    fontSize: Theme.of(context)
                                        .textTheme
                                        .headline6
                                        ?.fontSize),
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                mainAxisSize: MainAxisSize.max,
                                children: (_food as Food)
                                    .recipe
                                    .steps
                                    .map(
                                      (e) => RichText(
                                        text: TextSpan(
                                            style:


                                                TextStyle(color: Colors.black),
                                            children: [
                                              TextSpan(
                                                  text: e.position.toString() +
                                                      '. ',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold)),
                                              TextSpan(text: e.text)
                                            ]),
                                      ),
                                    )
                                    .toList(),
                              )
                            ]),
                      ),
                      Expanded(
                        child: ClipRRect(
                            borderRadius: const BorderRadius.only(
                                bottomLeft: Radius.circular(16.0),
                                bottomRight: Radius.circular(16.0)),
                            child: CachedNetworkImage(
                              imageUrl: _food.thumbnail_url,
                              fit: BoxFit.cover,
                            )),
                      ),
                    ],
                  )),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildExpandablePanel(
      BuildContext context, String title, String body) {
    return ExpandableNotifier(
        initialExpanded: true,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expandable(
                  collapsed: Container(
                      child: ExpandableButton(
                          child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 16, top: 8, bottom: 8),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                //crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    title,
                                    textAlign: TextAlign.left,
                                    style: const TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 22,
                                    ),
                                  ),
                                  const Icon(Icons.arrow_drop_down),
                                ],
                              )))),
                  expanded: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            decoration: const BoxDecoration(
                              color: Colors.blue,
                            ),
                            child: ExpandableButton(
                                child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 16, top: 8, bottom: 8),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                //crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    title,
                                    textAlign: TextAlign.left,
                                    style: const TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 22,
                                      color: Colors.white,
                                    ),
                                  ),
                                  const Icon(Icons.arrow_drop_up,
                                      color: Colors.white)
                                ],
                              ),
                            ))),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 16, top: 8, bottom: 8),
                          child: Text(
                            body,
                            style: const TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        )
                      ]))
            ]));
  }
}

class FoodCompilationWidget extends AbstractFoodWidget {
  final ScrollController _scrollController = ScrollController();

  FoodCompilationWidget(FoodCompilation food, {Key? key})
      : super._(food, key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        SliverAppBar(
          expandedHeight: 400,
          flexibleSpace: FlexibleSpaceBar(
            title: Text(
              _food.title,
              textAlign: TextAlign.center,
            ),
            background: CachedNetworkImage(
              imageUrl: _food.thumbnail_url,
              fit: BoxFit.cover,
            ),
          ),
          //floating: true,
          pinned: true,
          //automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () => Navigator.pop(context),
            icon: const Icon(Icons.arrow_back),
          ),
        ),
        SliverGrid(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              mainAxisSpacing: 5,
              crossAxisSpacing: 1,
              crossAxisCount: (Responsive.isMobile(context))
                  ? 2
                  : (Responsive.isTablet(context))
                      ? 3
                      : 4),
          delegate: SliverChildBuilderDelegate((context, index) {
            return _buildFoodItem(
                (_food as FoodCompilation).list[index], context);
          }, childCount: (_food as FoodCompilation).list.length),
        )
      ],
    );
  }

  Widget _buildFoodItem(AbstractFood item, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 24.0, right: 24.0, top: 24),
      child: InkWell(
          splashColor: Colors.transparent,
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        AbstractFoodWidget(item)));
          },
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(16.0)),
            child: Stack(
              alignment: Alignment.topCenter,
              children: [
                CachedNetworkImage(
                  imageUrl: item.thumbnail_url,
                  fit: BoxFit.fill,
                ),
                Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                        Colors.grey.withOpacity(1.0),
                        Colors.grey.withOpacity(0.0)
                      ])),
                  child: Text(
                    item.title,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 22,
                    ),
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
