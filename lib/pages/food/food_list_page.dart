import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:project_test/controllers/favorite_food_controller.dart';
import 'package:project_test/controllers/menu_controller.dart';
import 'package:project_test/pages/food/food_page.dart';
import 'package:project_test/pages/home/header_bar.dart';
import 'package:project_test/pages/home/menu.dart';

import '../../constants.dart';
import '../../models/food.dart';
import '../../controllers/food_controller.dart';
import '../../responsive.dart';
import 'favorite_widget.dart';

class FoodListPage extends StatefulWidget {
  const FoodListPage({Key? key}) : super(key: key);

  @override
  _PostListPageState createState() => _PostListPageState();
}

class _PostListPageState extends StateMVC {
  late final FoodController _foodController;
  late final ScrollController _foodListScrollController;

  _PostListPageState() : super(FoodController()) {
    _foodController = controller as FoodController;
    _foodListScrollController = ScrollController()
      ..addListener(_scrollListener);
  }

  @override
  void initState() {
    super.initState();
    _foodController.init();
  }

  @override
  void dispose() {
    _foodListScrollController.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildContext(context),
    );
  }

  Widget _buildContext(BuildContext context) {
    final state = _foodController.currentState;
    if (state is FoodResultInitialization) {
      return const Center(
        child: CircularProgressIndicator(),
      );
    } else {
      final foodList = _foodController.foodList.foods;
      var size = MediaQuery.of(context).size;

      final double itemHeight =
          (size.height - kToolbarHeight - kBottomNavigationBarHeight - 24);
      final double itemWidth = size.width / 2;

      return CustomScrollView(
        controller: _foodListScrollController,
        slivers: [
          SliverAppBar(
            flexibleSpace: const FlexibleSpaceBar(
              title: Text(
                'Food list',
                textAlign: TextAlign.center,
              ),
            ),
            //floating: true,
            pinned: true,
            //automaticallyImplyLeading: false,
            leading: IconButton(
              onPressed: () => MenuController().openMenu(),
              icon: const Icon(Icons.menu),
            ),
          ),
          SliverGrid(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                mainAxisSpacing: 5,
                crossAxisSpacing: 1,
                crossAxisCount: (Responsive.isMobile(context))
                    ? 2
                    : (Responsive.isTablet(context))
                    ? 3
                    : 4),
            delegate: SliverChildBuilderDelegate((context, index) {
              return _buildFoodItem(foodList[index]);
            }, childCount: foodList.length),
          ),
          if (state is FoodResultLoading)
            const SliverToBoxAdapter(
              child: const Center(
                child: CircularProgressIndicator(),
              ),
            )
        ],
      );
    }
  }

  Widget _buildFoodItem(AbstractFood post) {
    return Container(
      padding: const EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 16),
      child: InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => FoodPage(food: post),
                  fullscreenDialog: true));
        },
        splashColor: Colors.transparent,
        child: Container(
          decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(16.0)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.6),
                  offset: const Offset(4, 4),
                  blurRadius: 16,
                )
              ]),
          child: ClipRRect(
              borderRadius: const BorderRadius.all(Radius.circular(16.0)),
              child: Stack(
                children: [
                  //Column(
                  //children: [
                  AspectRatio(
                      aspectRatio: 1.0,
                      child: CachedNetworkImage(
                        imageUrl: post.thumbnail_url,
                        fit: BoxFit.cover,
                      )),
                  Container(
                    decoration: BoxDecoration(
                        //color: Colors.white.withOpacity(0.1),
                        gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Colors.white.withOpacity(0.7),
                        Colors.white.withOpacity(0.7)
                      ],
                    )),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                            child: Container(
                                child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 16, top: 8, bottom: 8),
                                    child: Text(
                                      post.title,
                                      textAlign: TextAlign.left,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 22,
                                      ),
                                    )))),
                        Container(
                          alignment: Alignment.centerRight,
                          width: AppBar().preferredSize.height + 40,
                          height: AppBar().preferredSize.height,
                          child: Material(
                            color: Colors.transparent,
                            child: FavoriteWidget(post),
                          ),
                        )
                      ],
                    ),
                  ),
                  //],
                  //)
                ],
              )),
        ),
      ),
    );
  }

  void _scrollListener() {
    final state = _foodController.currentState;
    if (state is! FoodResultSuccess) {
      return;
    }
    if (_foodListScrollController.position.extentAfter < 100) {
      final length = (state as FoodResultSuccess).postList.foods.length;
      _foodController.update(offset: length);
    }
  }

}
