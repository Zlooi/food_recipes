
import 'package:flutter/material.dart';
import 'package:project_test/models/food_filter.dart';
import 'package:project_test/pages/food/time_range_slider_view.dart';

class FoodFilterPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _FoodFilterState();

}

class _FoodFilterState extends State<FoodFilterPage> {
  List<FoodFilter> foodFilters = FoodFilter.defaultFiters;

  RangeValues _rangeValues = const RangeValues(0, 300);
  double _defaultValue = 30;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Column(
        children: [
          getAppBarUI(),
          Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    timeBarFilter(),
                    const Divider(height: 1),
                    getFiltersWidget()
                  ],
                ),

              )
          )

        ],

      ),
    );
  }

  Widget timeBarFilter() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Text(
            'Required time',
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Colors.grey,
                fontSize: MediaQuery.of(context).size.width > 360 ? 18 : 16,
                fontWeight: FontWeight.normal),
          ),
        ),
        TimeRangeSliderView(
          values: _rangeValues,
          onChangeRangeValues: (RangeValues values) {
            _rangeValues = values;
          },
        ),
        const SizedBox(
          height: 8,
        )
      ],
    );
  }

  Widget getFiltersWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(16),
          child: Text(
            'Filters',
            textAlign: TextAlign.left,
            style: TextStyle(
              color: Colors.grey,
              fontSize: MediaQuery.of(context).size.width > 360 ? 18 : 16,
              fontWeight: FontWeight.normal
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 16, right: 16),
          child: Column(
            children: getFiltersList(),
          ),
        ),
        const SizedBox(
          height: 8,
        )
      ],
    );
  }

  List<Widget> getFiltersList() {
    final List<Widget> list = [];
    int count = 0;
    const int columnCount = 3;
    for(int i = 0; i < foodFilters.length / columnCount; i++){
      final List<Widget> listUI = [];
      for(int j = 0; j < columnCount; j++) {
        try {
          final FoodFilter filter = foodFilters[count];
          listUI.add(Expanded(
              child: Row(
                children: [
                  Material(
                    color: Colors.transparent,
                    child: InkWell(
                      borderRadius: const BorderRadius.all(Radius.circular(4.0)),
                      onTap: () {
                        setState(() {
                          filter.activated = !filter.activated;
                        });
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Icon(
                              filter.activated ? Icons.check_box : Icons.check_box_outline_blank,
                              color:  filter.activated ? Colors.blue : Colors.grey,
                            ),
                            const SizedBox(
                              width: 4,
                            ),
                            Text(
                              filter.displayName,
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              )
          ));
          if(count < foodFilters.length - 1) {
            count++;
          } else {
            break;
          }
        }
        catch (error) {
          print(error);
        }
      }
      list.add(Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: listUI,
      ));
    }
    return list;
  }

  Widget getAppBarUI() {
    return Container(
      decoration: BoxDecoration(
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              offset: const Offset(0, 2),
              blurRadius: 4.0),
        ],
      ),
      child: Padding(
        padding: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top, left: 8, right: 8),
        child: Row(
          children: <Widget>[
            Container(
              alignment: Alignment.centerLeft,
              width: AppBar().preferredSize.height + 40,
              height: AppBar().preferredSize.height,
              child: Material(
                color: Colors.transparent,
                child: InkWell(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(32.0),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.close),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Center(
                child: Text(
                  'Filters',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 22,
                  ),
                ),
              ),
            ),
            Container(
              width: AppBar().preferredSize.height + 40,
              height: AppBar().preferredSize.height,
            )
          ],
        ),
      ),
    );
  }

}