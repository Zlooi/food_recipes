import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:project_test/models/favorite_food.dart';
import 'package:project_test/pages/food/favorite_widget.dart';
import 'package:project_test/pages/food/food_page.dart';
import 'package:project_test/pages/home/header_bar.dart';

import '../../controllers/favorite_food_controller.dart';
import '../../models/food.dart';

class FavoriteListPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _FavoriteListPageState();
}

class _FavoriteListPageState extends StateMVC {
  late final FavoriteFoodController _favoriteFoodController;

  _FavoriteListPageState() : super(FavoriteFoodController()) {
    _favoriteFoodController = controller as FavoriteFoodController;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          HeaderBar(title: 'Favorite'),
          Expanded(child: _buildContext())
        ],
      )
    );
  }

  Widget _buildContext() {
    FavoriteFoodList foodList = _favoriteFoodController.favoriteList;
    return Padding(
      padding: const EdgeInsets.all(10),
      child: foodList.list.isEmpty
          ? Container(
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset("assets/robber_flat.png"),
                  const Text(
                    "Someone has stolen your favorite dishes...",
                  )
                ],
              ),
            )
          : ListView.builder(
              itemCount: foodList.list.length,
              itemBuilder: (context, index) {
                return _buildPostItem(foodList.list[index]);
              },
            ),
    );
  }

  Widget _buildPostItem(AbstractFood food) {
    return Padding(
      padding: const EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 16),
      child: InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => FoodPage(food: food),
                fullscreenDialog: true
              )
          );
        },
        splashColor: Colors.transparent,
        child: Container(
          decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(16.0)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.6),
                  offset: const Offset(4, 4),
                  blurRadius: 16,
                )
              ]),
          child: ClipRRect(
              borderRadius: const BorderRadius.all(Radius.circular(16.0)),
              child: Stack(
                children: [
                  //Column(
                  //children: [
                  AspectRatio(
                      aspectRatio: 2.0,
                      child: CachedNetworkImage(
                        imageUrl: food.thumbnail_url,
                        fit: BoxFit.cover,
                      )),
                  Container(
                    decoration: BoxDecoration(
                        //color: Colors.white.withOpacity(0.1),
                        gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Colors.white.withOpacity(0.7),
                        Colors.white.withOpacity(0.7)
                      ],
                    )),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                            child: Container(
                                child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 16, top: 8, bottom: 8),
                                    child: Text(
                                      food.title,
                                      textAlign: TextAlign.left,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 22,
                                      ),
                                    )))),
                        Container(
                          alignment: Alignment.centerRight,
                          width: AppBar().preferredSize.height + 40,
                          height: AppBar().preferredSize.height,
                          child: Material(
                            color: Colors.transparent,
                            child: FavoriteWidget(food),
                          ),
                        )
                      ],
                    ),
                  ),
                  //],
                  //)
                ],
              )),
        ),
      ),
    );
  }
}
