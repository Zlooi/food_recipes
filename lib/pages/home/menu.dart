import 'package:flutter/material.dart';

class Menu extends StatelessWidget {
  const Menu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
              child: Image.asset('assets/avatar_flat.png'),
          ),
          DrawerListItem(
            title: 'Profile',
            image: Icons.face,
            callback: () {},
          ),
          DrawerListItem(
            title: 'Recipes',
            image: Icons.fastfood_rounded,
            callback: () {},
          ),
          DrawerListItem(
            title: 'Favorite',
            image: Icons.favorite_sharp,
            callback: () {},
          ),
          DrawerListItem(
            title: 'Settings',
            image: Icons.settings,
            callback: () {},
          )
        ],
      ),
    );
  }
  
}

class DrawerListItem extends StatelessWidget {
  final String title;
  final IconData image;
  final VoidCallback callback;

  const DrawerListItem({
    Key? key,
    required this.title,
    required this.image,
    required this.callback
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(title),
      leading: Icon(image),
      onTap: callback,
    );
  }
  
  
}