import 'package:flutter/material.dart';
import 'package:project_test/pages/food/favorite_list_page.dart';
import 'package:project_test/pages/food/food_list_page.dart';
import '../../models/tab.dart';

class TabNavigator extends StatelessWidget{
  final GlobalKey<NavigatorState> navigatorKey;
  final TabItem tabItem;

  TabNavigator({required this.navigatorKey, required this.tabItem});

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigatorKey,
      initialRoute: '/',
      onGenerateRoute: (routeSettings) {
        Widget currentPage;
        if(tabItem == TabItem.FOOD){
          currentPage = FoodListPage();
        } else {
          currentPage = FavoriteListPage();
        }
        return MaterialPageRoute(builder: (context) => currentPage);
      }
    );
  }

}