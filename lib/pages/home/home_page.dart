import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../../models/tab.dart';
import '../../controllers/home_controller.dart';
import '../../responsive.dart';
import 'bottom_navigation.dart';

import 'header_bar.dart';
import 'menu.dart';
import 'tab_navigator.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends StateMVC {

  HomeController? _homeController;

  _HomePageState() : super(HomeController()){
    _homeController = HomeController.controller!;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if(_homeController!.currentTab == TabItem.FOOD){
          return true;
        }
        else if(_homeController!.currentTab == TabItem.FAVORITES){
          _homeController!.selectTab(TabItem.FAVORITES);
          return false;
        }
        else {
          _homeController!.selectTab(TabItem.FOOD);
          return false;
        }
      },
      child: Scaffold(
        drawer: Menu(),
        body: SafeArea(
          child: Row(
            children: [
              if (Responsive.isDesktop(context))
                const Expanded(
                  flex: 1,
                  child: Menu(),
                ),
              Expanded(
                flex: 5,
                child: Column(children: [
                  //HeaderBar(),
                  Expanded(
                    child: Stack(children: <Widget>[
                      _buildOffstageNavigator(TabItem.FOOD),
                      _buildOffstageNavigator(TabItem.FAVORITES),
                    ]),
                  )
                ]),
              ),
            ],
          ),
        ),
        bottomNavigationBar: MyBottomNavigationBar(
          currentTab: _homeController!.currentTab,
          onSelectTab: _homeController!.selectTab,
        ),
        key: HomeController().scaffoldKey,
      )
    );
  }
  
  Widget _buildOffstageNavigator(TabItem tabItem) {
    return Offstage(
      offstage: _homeController!.currentTab != tabItem,
      child: TabNavigator(
        navigatorKey: _homeController!.navigatorKeys[tabItem] as GlobalKey<NavigatorState>,
        tabItem: tabItem,
      ),
    );
  }

  

}