import 'package:flutter/material.dart';
import '../../models/tab.dart';

const Map<TabItem, MyTab> tabs = {
  TabItem.FOOD : const MyTab(name: "Food", color: Colors.deepOrange, icon: Icons.fastfood_rounded),
  TabItem.FAVORITES : const MyTab(name: "Favorites", color: Colors.red, icon: Icons.favorite),
};

class MyBottomNavigationBar extends StatelessWidget{
  final TabItem currentTab;
  final ValueChanged<TabItem> onSelectTab;
  MyBottomNavigationBar({required this.currentTab, required this.onSelectTab});

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      selectedItemColor: _colorTabMatching(currentTab),
      selectedFontSize: 13,
      unselectedItemColor: Colors.grey,
      type: BottomNavigationBarType.fixed,
      currentIndex: currentTab.index,
      items: [
        _buildItem(TabItem.FOOD),
        _buildItem(TabItem.FAVORITES),
      ],
      onTap: (index) => onSelectTab(
        TabItem.values[index]
      ),
    );
  }

  BottomNavigationBarItem _buildItem(TabItem item){
    return BottomNavigationBarItem(
        icon: Icon(
            _iconTabMatching(item),
            color: _colorTabMatching(item),
        ),
        label: tabs[item]!.name
    );
  }

  IconData _iconTabMatching(TabItem item) => tabs[item]!.icon;

  Color _colorTabMatching(TabItem item) => currentTab == item? tabs[item]!.color : Colors.grey;
}