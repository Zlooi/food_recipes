import 'package:flutter/cupertino.dart';

class IngredientsList {
  final List<String> _ingredients = [];

  List<String> get ingredients => _ingredients;

  IngredientsList.fromJson(List<dynamic> sectionsJson) {
    for (var jsonItem in sectionsJson) {
      if (jsonItem["components"] != null) {
        for (var component in jsonItem["components"]) {
          ingredients.add(component["raw_text"]);
        }
      }
    }
  }
  IngredientsList();
}

class RecipeStep {
  final int _startTime;
  final int _endTime;
  final String _appliance;
  final int _temperature;
  final int _id;
  final int _position;
  final String _text;

  int get startTime => _startTime;
  int get endTime => _endTime;
  String get appliance => _appliance;
  int get temperature => _temperature;
  int get id => _id;
  int get position => _position;
  String get text => _text;

  RecipeStep.fromJson(Map<String, dynamic> json)
      : _startTime = json["start_time"] ?? 0,
        _endTime = json["end_time"] ?? 0,
        _appliance = json["appliance"] ?? "",
        _temperature = json["temperature"] ?? 0,
        _id = json["id"] ?? -1,
        _position = json["position"] ?? -1,
        _text = json["display_text"] ?? "";
}

class Recipe {
  final List<RecipeStep> _steps = [];
  List<RecipeStep> get steps => _steps;

  String get recipe {
    String text = "";
    for (int i = 0; i < steps.length; i++) {
      text += "Step ${i + 1}\n${steps[i]._text}\n";
    }
    return text;
  }

  Recipe.fromJson(List<dynamic> json) {
    for (var step in json) {
      steps.add(RecipeStep.fromJson(step));
    }
  }
  Recipe();
}

abstract class AbstractFood extends ChangeNotifier {
  final String _canonicalId;
  final int _id;
  final String _title;
  final String _body;
  final String _thumbnail_url;
  final String _videoUrl;
  bool _isFavorite = false;

  String get canonicalId => _canonicalId;
  int get id => _id;
  String get title => _title;
  String get body => _body;
  String get thumbnail_url => _thumbnail_url;
  String get videoUrl => _videoUrl;
  bool get isFavorite => _isFavorite;

  set isFavorite(bool favorite) {
    _isFavorite = favorite;
    notifyListeners();
  }

  factory AbstractFood.fromJson(Map<String, dynamic> json) {
    var type = json["canonical_id"].split(':').first;
    if (type == 'recipe') {
      return Food.fromJson(json);
    } else if (type == 'compilation') {
      return FoodCompilation.fromJson(json);
    }
    throw UnimplementedError();
  }

  AbstractFood._fromJson(Map<String, dynamic> json)
      : _canonicalId = json["canonical_id"],
        _id = json["show_id"],
        _title = json["name"],
        _body = json["description"] ?? "",
        _thumbnail_url = json["thumbnail_url"],
        _videoUrl = json["video_url"] ?? "";
}

class Food extends AbstractFood {
  late final IngredientsList _ingredientsList;
  late final Recipe _recipe;

  IngredientsList get ingredientsList => _ingredientsList;
  Recipe get recipe => _recipe;

  Food.fromJson(Map<String, dynamic> json) : super._fromJson(json) {
    if (json["sections"] != null) {
      _ingredientsList = IngredientsList.fromJson(json["sections"]);
    } else {
      _ingredientsList = IngredientsList();
    }
    if (json["instructions"] != null) {
      _recipe = Recipe.fromJson(json["instructions"]);
    } else {
      _recipe = Recipe();
    }
  }
}

class FoodCompilation extends AbstractFood {
  final List<AbstractFood> list = [];

  FoodCompilation.fromJson(Map<String, dynamic> json) : super._fromJson(json) {
    for(var recipe in json['recipes']){
      list.add(AbstractFood.fromJson(recipe));
    }
  }
}

//food data list
class FoodList {
  final List<AbstractFood> foods = [];

  FoodList();

  FoodList.fromJson(Map<String, dynamic> json) {
    addFromJson(json);
  }

  void addFromJson(Map<String, dynamic> json) {
    for (var jsonItem in json["results"]) {
      foods.add(AbstractFood.fromJson(jsonItem));
    }
  }

  void add(FoodList foodList) {
    foods.addAll(foodList.foods);
  }
}

abstract class FoodResult {}

class FoodResultSuccess extends FoodResult {
  final FoodList postList;
  FoodResultSuccess(this.postList);
}

class FoodResultFailure extends FoodResult {
  final String error;
  FoodResultFailure(this.error);
}

class FoodResultLoading extends FoodResult {
  FoodResultLoading();
}

class FoodResultInitialization extends FoodResult {
  FoodResultInitialization();
}