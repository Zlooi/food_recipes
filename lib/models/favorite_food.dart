

import 'package:project_test/models/food.dart';

class FavoriteFoodList {
  final List<AbstractFood> favoriteFood = [];
  static FavoriteFoodList? _this;

  List<AbstractFood> get list => favoriteFood;
  FavoriteFoodList get favoriteFoodList => _this!;

  factory FavoriteFoodList(){
    _this ??= FavoriteFoodList._();
    return _this!;
  }

  FavoriteFoodList._();

  void addFavorite(AbstractFood food){
    favoriteFood.add(food);
  }

  void removeFavorite(AbstractFood food){
    favoriteFood.remove(food);
  }
}