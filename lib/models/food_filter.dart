import '../Repository/repository.dart';

class FoodFilter {
  final String name;
  final String displayName;
  final String type;
  final int id;
  bool activated;

  FoodFilter(
      {this.name = "",
      this.displayName = "",
      this.type = "",
      this.id = -1,
      this.activated = false});

  FoodFilter.fromJson(Map<String, dynamic> json)
      : name = json["name"],
        displayName = json["display_name"],
        type = json["type"],
        id = json["id"],
        activated = false;

  static List<FoodFilter> defaultFiters = [
    FoodFilter(
      id: 64447,
      type: "cuisine",
      name: "british",
      displayName: "British",
    ),
    FoodFilter(
      id: 64453,
      type: "cuisine",
      name: "italian",
      displayName: "Italian",
    ),
    FoodFilter(
      name: "mexican",
      displayName: "Mexican",
      id: 64457,
      type: "cuisine",
    ),
    FoodFilter(
      id: 64458,
      type: "cuisine",
      name: "middle_eastern",
      displayName: "Middle Eastern",
    ),
    FoodFilter(
      name: "dairy_free",
      displayName: "Dairy-Free",
      id: 64463,
      type: "dietary",
    ),
    FoodFilter(
      id: 64468,
      type: "dietary",
      name: "vegan",
      displayName: "Vegan",
    ),
    FoodFilter(
      id: 64473,
      type: "holiday",
      name: "christmas",
      displayName: "Christmas",
    ),
    FoodFilter(
      name: "fourth_of_july",
      displayName: "Fourth of July",
      id: 64475,
      type: "holiday",
    ),
    FoodFilter(
      id: 64476,
      type: "holiday",
      name: "halloween",
      displayName: "Halloween",
    ),
    FoodFilter(
      id: 64480,
      type: "holiday",
      name: "valentines_day",
      displayName: "Valentine's Day",
    ),
    FoodFilter(
      id: 64481,
      type: "meal",
      name: "appetizers",
      displayName: "Appetizers",
    ),
    FoodFilter(
      id: 64491,
      type: "meal",
      name: "snacks",
      displayName: "Snacks",
    ),
    FoodFilter(type: "method", name: "grill", displayName: "Grill", id: 64494),
    FoodFilter(
      id: 64509,
      type: "seasonal",
      name: "spring",
      displayName: "Spring",
    ),
    FoodFilter(
      id: 65839,
      type: "appliance",
      name: "broiler",
      displayName: "Broiler",
    ),
    FoodFilter(
      id: 65842,
      type: "appliance",
      name: "food_processor",
      displayName: "Food Processor",
    ),
    FoodFilter(
      id: 65852,
      type: "dish_style",
      name: "mashup",
      displayName: "Mashup",
    ),
    FoodFilter(
      id: 65856,
      type: "dish_style",
      name: "stuffed",
      displayName: "Stuffed",
    ),
    FoodFilter(
      id: 1247768,
      type: "equipment",
      name: "cake_pan",
      displayName: "Cake Pan",
    ),
    FoodFilter(
      id: 1247774,
      type: "equipment",
      name: "lollipop_sticks",
      displayName: "Lollipop Sticks",
    ),
    FoodFilter(
      displayName: "Paper Bowls",
      id: 1247776,
      type: "equipment",
      name: "paper_bowls",
    ),
    FoodFilter(
      id: 1247777,
      type: "equipment",
      name: "paper_cups",
      displayName: "Paper Cups",
    ),
    FoodFilter(
      id: 1247779,
      type: "equipment",
      name: "paper_plates",
      displayName: "Paper Plates",
    ),
    FoodFilter(
      id: 1247785,
      type: "equipment",
      name: "pyrex",
      displayName: "Pyrex",
    ),
    FoodFilter(
      id: 1247787,
      type: "equipment",
      name: "saute_pan",
      displayName: "Saute Pan",
    ),
    FoodFilter(
      displayName: "Tongs",
      id: 1247790,
      type: "equipment",
      name: "tongs",
    ),
    FoodFilter(
      name: "cutting_board",
      displayName: "Cutting Board",
      id: 1280503,
      type: "equipment",
    ),
    FoodFilter(
      id: 1280504,
      type: "equipment",
      name: "fish_spatula",
      displayName: "Fish Spatula",
    ),
    FoodFilter(
      displayName: "Microplane",
      id: 1280509,
      type: "equipment",
      name: "microplane",
    ),
    FoodFilter(
      name: "epoca_walmart",
      displayName: "Epoca-Walmart",
      id: 1691104,
      type: "business_tags",
    ),
    FoodFilter(
      displayName: "Eggs",
      id: 2651754,
      type: "business_tags",
      name: "one_top_app_eggs",
    ),
    FoodFilter(
      id: 2651756,
      type: "business_tags",
      name: "one_top_app_veggies",
      displayName: "Veggies",
    ),
    FoodFilter(
        type: "business_tags",
        name: "one_top_app_sides",
        displayName: "Sides",
        id: 2651757),
    FoodFilter(
      id: 3801552,
      type: "dietary",
      name: "pescatarian",
      displayName: "Pescatarian",
    ),
    FoodFilter(
      id: 4767335,
      type: "appliance",
      name: "pressure_cooker",
      displayName: "Pressure Cooker",
    ),
    FoodFilter(
      name: "mccormick_easy_dinner",
      displayName: "McCormick Easy Dinner",
      id: 5143247,
      type: "business_tags",
    ),
    FoodFilter(
      id: 5285641,
      type: "dietary",
      name: "contains_alcohol",
      displayName: "Contains Alcohol",
    ),
    FoodFilter(
      displayName: "Quarantine Cooking: Baking",
      id: 5812431,
      type: "feature_page",
      name: "qfp_baking",
    ),
    FoodFilter(
      id: 5831534,
      type: "business_tags",
      name: "best_of_tasty",
      displayName: "Best of Tasty",
    ),
    FoodFilter(
      displayName: "Beyond Red Blend",
      id: 5923246,
      type: "business_tags",
      name: "beyond_red_blend",
    ),
    FoodFilter(
      id: 5949791,
      type: "business_tags",
      name: "light_bites",
      displayName: "Light Bites",
    ),
    FoodFilter(
      name: "tasty_s_5th_birthday_savory",
      displayName: "Tasty's 5th Birthday: Savory",
      id: 5993379,
      type: "feature_page",
    ),
    FoodFilter(
      id: 6337137,
      type: "business_tags",
      name: "holiday_treats",
      displayName: "Holiday Treats",
    ),
    FoodFilter(
      id: 6361741,
      type: "feature_page",
      name: "peppermint_pattie",
      displayName: "Peppermint Pattie",
    ),
    FoodFilter(
      id: 6361809,
      type: "business_tags",
      name: "raspberry_rose",
      displayName: "Raspberry Rosé",
    ),
    FoodFilter(
      id: 6361812,
      type: "business_tags",
      name: "tasty_dinner_kits",
      displayName: "Tasty Dinner Kits",
    ),
    FoodFilter(
      displayName: "Tasty Holiday: Hanukkah",
      id: 6389773,
      type: "feature_page",
      name: "tasty_holiday_hanukkah",
    ),
    FoodFilter(
      id: 6389774,
      type: "feature_page",
      name: "tasty_holiday_nye",
      displayName: "Tasty Holiday: NYE",
    ),
    FoodFilter(
      id: 6543464,
      type: "feature_page",
      name: "nynm_protein",
      displayName: "NYNM Protein",
    ),
    FoodFilter(
      id: 6543465,
      type: "feature_page",
      name: "nynm_desserts",
      displayName: "NYNM Desserts",
    ),
    FoodFilter(
      name: "mc_breakfast_healthy",
      displayName: "MC Breakfast Healthy",
      id: 6683352,
      type: "feature_page",
    ),
    FoodFilter(
      id: 6711300,
      type: "holiday",
      name: "st_patrick_s_day",
      displayName: "St. Patrick's Day",
    ),
    FoodFilter(
      displayName: "Tips Viral",
      id: 6718867,
      type: "feature_page",
      name: "tips_viral",
    ),
    FoodFilter(
      id: 6718869,
      type: "feature_page",
      name: "spring_eat",
      displayName: "Spring Eat",
    ),
    FoodFilter(
      displayName: "Spring Holiday",
      id: 6718874,
      type: "feature_page",
      name: "spring_holiday",
    ),
    FoodFilter(
      id: 6830249,
      type: "feature_page",
      name: "budget_plans",
      displayName: "Budget Plans",
    ),
    FoodFilter(
      id: 6830250,
      type: "feature_page",
      name: "budget_expert",
      displayName: "Budget Expert",
    ),
    FoodFilter(
      id: 6854261,
      type: "holiday",
      name: "cinco_de_mayo",
      displayName: "Cinco de Mayo",
    ),
    FoodFilter(
        type: "cuisine",
        name: "dominican",
        displayName: "Dominican",
        id: 6953047),
    FoodFilter(
        type: "cuisine",
        name: "puerto_rican",
        displayName: "Puerto Rican",
        id: 6953050),
    FoodFilter(
      id: 6953051,
      type: "cuisine",
      name: "soul_food",
      displayName: "Soul Food",
    ),
    FoodFilter(
      displayName: "McCormick UGC One Pot Pasta",
      id: 6986105,
      type: "feature_page",
      name: "mccormick_ugc_one_pot_pasta",
    ),
    FoodFilter(
      id: 7041320,
      type: "feature_page",
      name: "summer_eat",
      displayName: "Summer Eat",
    ),
    FoodFilter(
      id: 7041321,
      type: "feature_page",
      name: "summer_drink",
      displayName: "Summer Drink",
    ),
    FoodFilter(
        type: "feature_page",
        name: "fourth_picks",
        displayName: "Fourth Picks",
        id: 7041336),
    FoodFilter(
      id: 7510780,
      type: "feature_page",
      name: "thanksgiving_slow",
      displayName: "Thanksgiving Slow",
    ),
    FoodFilter(
      id: 7559499,
      type: "feature_page",
      name: "shoppable_recipes_thanksgiving",
      displayName: "Shoppable Recipes Thanksgiving",
    ),
    FoodFilter(
      id: 7723151,
      type: "feature_page",
      name: "franks_handheld_bites",
      displayName: "Franks Handheld Bites",
    ),
    FoodFilter(
      id: 64451,
      type: "cuisine",
      name: "greek",
      displayName: "Greek",
    ),
    FoodFilter(
      id: 64452,
      type: "cuisine",
      name: "indian",
      displayName: "Indian",
    ),
    FoodFilter(
        type: "cuisine", name: "seafood", displayName: "Seafood", id: 64459),
    FoodFilter(
      name: "thai",
      displayName: "Thai",
      id: 64460,
      type: "cuisine",
    ),
    FoodFilter(
      id: 64467,
      type: "dietary",
      name: "low_carb",
      displayName: "Low-Carb",
    ),
    FoodFilter(
      id: 64470,
      type: "difficulty",
      name: "5_ingredients_or_less",
      displayName: "5 Ingredients or Less",
    ),
    FoodFilter(
      displayName: "Easy",
      id: 64471,
      type: "difficulty",
      name: "easy",
    ),
    FoodFilter(
      id: 64483,
      type: "meal",
      name: "breakfast",
      displayName: "Breakfast",
    ),
    FoodFilter(
      id: 64487,
      type: "meal",
      name: "drinks",
      displayName: "Drinks",
    ),
    FoodFilter(
      displayName: "Lunch",
      id: 64489,
      type: "meal",
      name: "lunch",
    ),
    FoodFilter(
      displayName: "Blender",
      id: 65838,
      type: "appliance",
      name: "blender",
    ),
    FoodFilter(
      id: 65846,
      type: "appliance",
      name: "oven",
      displayName: "Oven",
    ),
    FoodFilter(
      id: 65850,
      type: "dietary",
      name: "indulgent_sweets",
      displayName: "Indulgent Sweets",
    ),
    FoodFilter(
      name: "special_occasion",
      displayName: "Special Occasion",
      id: 188967,
      type: "occasion",
    ),
    FoodFilter(
      name: "ice_cube_tray",
      displayName: "Ice Cube Tray",
      id: 1247773,
      type: "equipment",
    ),
    FoodFilter(
      id: 1247778,
      type: "equipment",
      name: "paper_napkins",
      displayName: "Paper Napkins",
    ),
    FoodFilter(
      id: 1247780,
      type: "equipment",
      name: "parchment_paper",
      displayName: "Parchment Paper",
    ),
    FoodFilter(
      displayName: "Zipper Freezer Bags",
      id: 1247795,
      type: "equipment",
      name: "zipper_freezer_bags",
    ),
    FoodFilter(
      id: 1247796,
      type: "equipment",
      name: "zipper_storage_bags",
      displayName: "Zipper Storage Bags",
    ),
    FoodFilter(
      name: "baking_kit",
      displayName: "Baking Kit",
      id: 1280497,
      type: "business_tags",
    ),
    FoodFilter(
      id: 1280500,
      type: "equipment",
      name: "baking_pan",
      displayName: "Baking Pan",
    ),
    FoodFilter(
      id: 1280502,
      type: "equipment",
      name: "cooling_rack",
      displayName: "Cooling Rack",
    ),
    FoodFilter(
      id: 1280508,
      type: "equipment",
      name: "measuring_spoons",
      displayName: "Measuring Spoons",
    ),
    FoodFilter(
      id: 2651753,
      type: "business_tags",
      name: "one_top_app_meat",
      displayName: "Meat",
    ),
    FoodFilter(
      id: 3801551,
      type: "business_tags",
      name: "ice_cream_social",
      displayName: "Ice Cream Social",
    ),
    FoodFilter(
      id: 3802076,
      type: "holiday",
      name: "black_history_month",
      displayName: "Black History Month",
    ),
    FoodFilter(
      id: 3802078,
      type: "holiday",
      name: "pride_month",
      displayName: "Pride Month",
    ),
    FoodFilter(
        type: "business_tags",
        name: "mccormick_seasoned_pro",
        displayName: "McCormick Seasoned Pro",
        id: 3956651),
    FoodFilter(
      displayName: "Schwartz Seasoned Pro",
      id: 3956652,
      type: "business_tags",
      name: "schwartz_seasoned_pro",
    ),
    FoodFilter(
      id: 4708980,
      type: "business_tags",
      name: "walmart_holiday_bundle",
      displayName: "Walmart Holiday Bundle",
    ),
    FoodFilter(
        type: "business_tags",
        name: "tastyjunior",
        displayName: "tastyjunior",
        id: 4767341),
    FoodFilter(
      id: 5831533,
      type: "business_tags",
      name: "eko_video",
      displayName: "Eko Video",
    ),
  ];
}

class FoodFilterList {
  final List<FoodFilter> filters = [];
  static FoodFilterList? _this;

  static FoodFilterList? get foodFilterList => _this;

  factory FoodFilterList() {
    _this ??= FoodFilterList._();
    return _this!;
  }

  FoodFilterList._() {
    init();
  }

  void init() async {
    _this = await Repository.repository?.fetchTags();
  }

  FoodFilterList.fromJson(Map<String, dynamic> json) {
    for (var jsonItem in json["results"]) {
      filters.add(FoodFilter.fromJson(jsonItem));
    }
  }
}
