
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class HeaderBarState {
  final String _title;
  final bool _isSearchVisible;
  final IconData _primaryButton;

  String get title => _title;
  bool get isSearchVisible => _isSearchVisible;
  IconData get primaryButton => _primaryButton;

  HeaderBarState(
      {required title, required searchVisible, required primaryButton})
      : _title = title,
        _isSearchVisible = searchVisible,
        _primaryButton = primaryButton;
}


class MainHeaderBarState extends HeaderBarState {
  MainHeaderBarState({required title}) : super(title: title, searchVisible: true, primaryButton: Icons.menu);
}

class DetailsHeaderBarState extends HeaderBarState {
  DetailsHeaderBarState({required title}) : super(title: title, searchVisible: false, primaryButton: Icons.arrow_back);
}