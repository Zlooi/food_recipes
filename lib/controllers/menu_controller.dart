
import 'package:flutter/material.dart';

class MenuController  {
  final GlobalKey<ScaffoldState> _key = GlobalKey();

  GlobalKey<ScaffoldState> get key => _key;

  void openMenu(){
    ScaffoldState currentState = _key.currentState!;
    if(!currentState.isDrawerOpen){
      currentState.openDrawer();
    }
  }

}