import 'package:mvc_pattern/mvc_pattern.dart';
import '../Repository/repository.dart';
import '../models/food.dart';

class FoodController extends ControllerMVC {
  late final Repository repo;
  FoodList foodList;
  FoodResult currentState = FoodResultInitialization();

  FoodController() : foodList = FoodList() {
    repo = Repository.repository!;
  }

  void init() async {
    currentState = FoodResultInitialization();
    try {
      foodList = await repo.fetchPosts(
        offset: 0,
        size: 40
      );
      setState(() => currentState = FoodResultSuccess(foodList));
    }
    catch(error){
      setState(() => currentState = FoodResultFailure("Can't connect to server"));
      rethrow;
    }
  }

  void update({required int offset, int size = 40, String search = "", String tags = ""}) async {
    final FoodList foods;
    setState(() =>currentState = FoodResultLoading());
    try{
      foods = await repo.fetchPosts(
        offset: offset,
        search: search,
        size: size,
        tags: tags
      );
      foodList.add(foods);
      setState(() => currentState = FoodResultSuccess(foodList));
    }
    catch(error){
      setState(() => currentState = FoodResultFailure("Can't connect to server"));
    }
  }
}