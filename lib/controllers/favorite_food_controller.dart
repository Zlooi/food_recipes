

import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:project_test/models/favorite_food.dart';

import '../models/food.dart';

class FavoriteFoodController extends ControllerMVC{
  final FavoriteFoodList _favoriteFoodList;
  static FavoriteFoodController? _this;

  factory FavoriteFoodController() {
    _this ??= FavoriteFoodController._();
    return _this!;
  }

  FavoriteFoodController._() : _favoriteFoodList = FavoriteFoodList();

  FavoriteFoodList get favoriteList => _favoriteFoodList;

  void reverseFavorite(AbstractFood food) {
    setState(() {
      food.isFavorite = !food.isFavorite;
      if (food.isFavorite) {
        _favoriteFoodList.addFavorite(food);
      }
      else {
        _favoriteFoodList.removeFavorite(food);
      }
    });
  }
}