import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../models/tab.dart';

class HomeController extends ControllerMVC {
  static HomeController? _this;
  static HomeController? get controller => _this;

  factory HomeController(){
    _this ??= HomeController._();
    return _this!;
  }

  HomeController._();

  final navigatorKeys = {
    TabItem.FOOD: GlobalKey<NavigatorState>(),
    TabItem.FAVORITES: GlobalKey<NavigatorState>(),
  };

  var _currentTab = TabItem.FOOD;

  TabItem get currentTab => _currentTab;

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void openMenu(){
    ScaffoldState currentState = _scaffoldKey.currentState!;
    if(!currentState.isDrawerOpen){
      setState(() => currentState.openDrawer());
    }
  }

  void selectTab(TabItem tabItem){
    setState(() => _currentTab = tabItem);
  }



}