import 'dart:convert';
import 'package:http/http.dart' as http;
import '../models/food_filter.dart';
import '../models/food.dart';
import 'auth.dart';


class Repository {
  static Repository? _this;
  static Repository? get repository => _this;

  factory Repository(){
    _this ??= Repository._();
    return _this!;
  }

  Repository._();

  Future<FoodList> fetchPosts({
    required int offset,
    int size = 20,
    String tags = "",
    String search = "" }) async {
    //create url
    String urlString = "$rapidApiServer/recipes/list?from=$offset&size=20";
    if(tags.isNotEmpty) {
      urlString += "&tags=$tags";
    }
    if(search.isNotEmpty){
      urlString+= "&q=$search";
    }
    final url = Uri.parse(urlString);
    //GET request
    final responce = await http.get(url, headers: rapidApiHeaders);
    //check responce
    if(responce.statusCode == 200){
      return FoodList.fromJson(json.decode(const Utf8Decoder().convert(responce.bodyBytes)));
    } else {
      throw Exception("GET request to $url was failed");
    }
  }

  Future<FoodFilterList> fetchTags() async {
    String urlString = "$rapidApiServer/tags/list";
    final url = Uri.parse(urlString);

    //GET request
    final responce = await http.get(url, headers: rapidApiHeaders);
    //check responce
    if(responce.statusCode == 200){
      return FoodFilterList.fromJson(json.decode(responce.body));
    } else {
      throw Exception("Get request to $url was failed");
    }
  }
}
