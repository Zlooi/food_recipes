import 'package:flutter/material.dart';
import 'package:project_test/Repository/repository.dart';
import 'package:project_test/models/food_filter.dart';
import 'pages/home/home_page.dart';

void main() {
  Repository();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //debugShowCheckedModeBanner: false,
      title: 'Meal Recipe',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}